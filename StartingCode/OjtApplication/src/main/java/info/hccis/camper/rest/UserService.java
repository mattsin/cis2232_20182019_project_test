package info.hccis.camper.rest;

import com.google.gson.Gson;
import info.hccis.admin.dao.UserDAO;
import info.hccis.camper.model.DatabaseConnection;
import info.hccis.camper.model.jpa.User;
import info.hccis.ojt.data.springdatajpa.UserRepository;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/UserService")
public class UserService {
    
    @Resource
    private final UserRepository ur;

    /**
     * Note that dependency injection (constructor) is used here to provide the UserRepository 
     * object for use in this class.  
     * 
     * @param servletContext Context
     * @since 20180604
     * @author BJM
     */
    
    public UserService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.ur = applicationContext.getBean(UserRepository.class);
    }

    /**
     * This rest service will provide all users from the associated database.
     * @return json string containing all user information.
     * @since 20180604
     * @author BJM
     */
    
    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {

        ArrayList<User> users = UserDAO.getUsers(new DatabaseConnection());
        Gson gson = new Gson();

        int statusCode = 200;
        if (users.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(users);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

    /**
     * This rest service will provide all a user object from the associated database 
     * that has a matching username/password (hashed)
     * @return json string containing the user attributes.
     * @since 20180604
     * @author BJM
     */
    
    @GET
    @Path("/users/{username}/{password}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("username") String user, @PathParam("password") String password) {

        ArrayList<User> login = (ArrayList<User>) ur.findByUsername(user);

        System.out.println("Getting user info for username="+user);
        
        Gson gson = new Gson();
        //convert the user to JSON
        String temp;
        //check if any errors occurred
        int statusCode = 200;
        if (login.isEmpty() || !(login.get(0).getPassword().equalsIgnoreCase(password))) {
            //if the logins array is empty, or the password doesn't match, return a new null user
            temp = gson.toJson(new User());
        } else {
            //if the passwords match, then return the 
            // testing match
            System.out.println("DEBUG user password " + password);
            System.out.println("DEBUG db password " + login.get(0).getPassword());
            temp = gson.toJson(login.get(0));
        }

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

}
