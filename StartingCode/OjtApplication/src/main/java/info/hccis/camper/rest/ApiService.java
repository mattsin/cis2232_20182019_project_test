package info.hccis.camper.rest;

import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/api")
public class ApiService {

    public ApiService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServices() {
        ArrayList<String> services = new ArrayList();

        String output = "Services available\r\n\r\n";
        output += "__________________________StudentService_________________________\r\n";
        output += "Get all students\r\n";
        output += "           /students/\r\n";
        output += "example:   http://hccis.info:8080/ojt/rest/StudentService/students/\r\n\r\n";
        output += "Get a particular student\r\n";
        output += "           /students/studentEmail\r\n";
        output += "example:   http://hccis.info:8080/ojt/rest/StudentService/students/bjmaclean@hollandcollege.com\r\n\r\n";
        output += "__________________________BusinessService_________________________\r\n";
        output += "           /business\r\n";
        output += "example    http://hccis.info:8080/ojt/rest/BusinessService/business\r\n\r\n";
        output += "Get a particular business\r\n";
        output += "           /business/business+name\r\n";
        output += "example:   http://hccis.info:8080/ojt/rest/BusinessService/business/Holland+College\r\n\r\n";
        output += "__________________________UserService_________________________\r\n";
        output += "Login user (returns user object as json)\r\n";
        output += "           /user/userName/hashedPassword\r\n";
        output += "example    http://hccis.info:8080/ojt/rest/UserService/users/bmaclean/202cb962ac59075b964b07152d234b70\r\n\r\n";
        output += "Get all users";
        output += "           /users\r\n";
        output += "example    http://hccis.info:8080/ojt/rest/UserService/users\r\n";

        return Response.status(200).entity(output).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
}
