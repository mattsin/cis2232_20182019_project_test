package info.hccis.admin.dao;

import info.hccis.camper.model.DatabaseConnection;
import info.hccis.camper.model.jpa.User;
import info.hccis.camper.util.Utility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UserDAO {

    /**
     * This method will get the user type code for a given db based on the 
     * username and password parameters.
     *
     * @since 20180524
     * @author BJM
     *
     * @param database
     * @param username
     * @param password
     * @return the associated user type code (0 if not found)
     */
    
    public static String getUserTypeCode(String database, String username, String password) {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql;

        try {
            //******************************************************************
            //connect to database
            //******************************************************************
            String propFileName = "spring.data-access";
            ResourceBundle rb = ResourceBundle.getBundle(propFileName);
            String dbUserName = rb.getString("jdbc.username");
            String dbPassword = rb.getString("jdbc.password");
            System.out.println("BJM-Set the database to "+database);
            DatabaseConnection newConnection = new DatabaseConnection(database, dbUserName, dbPassword);
            conn = ConnectionUtils.getDBConnection(newConnection);

            //******************************************************************
            //create query to get usertypecode
            //******************************************************************

            sql = "SELECT `userTypeCode` FROM `UserAccess` WHERE `username` = '" + username + "' AND `password`= '" + Utility.getHashPassword(password) + "'";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            //******************************************************************
            //get data if statement returns a value
            //******************************************************************
            String userTypeCode = null;
            while (rs.next()) {
                userTypeCode = Integer.toString(rs.getInt("userTypeCode"));
            }

            //return if data found
            if (userTypeCode != null) {
                return userTypeCode;
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            System.out.println("ERROR GETTING: User Type Code"
                    + "WHY: " + errorMessage);
        } finally {
            DbUtils.close(ps, conn);
        }
        //if data isn't found return 0
        return "0";
    }

     /**
     * This method will get the users from the database specified in the 
     * database connection object.
     *
     * @since 20180524
     * @author BJM
     *
     * @param DatabaseConnection
     * @return list of users
     */
    
    public static ArrayList<User> getUsers(DatabaseConnection databaseConnection) {
        ArrayList<User> users = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);

            sql = "SELECT * FROM `User` order by userId";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setUserId(rs.getInt("userId"));
                user.setUsername(rs.getString("username"));
                user.setLastName(rs.getString("lastName"));
                user.setFirstName(rs.getString("firstName"));
                user.setUserTypeCode(rs.getInt("userTypeCode"));
                user.setUserTypeDescription(CodeValueDAO.getCodeValueDescription(databaseConnection, 1, user.getUserTypeCode()));
                user.setCreatedDateTime(rs.getString("createdDateTime"));
                users.add(user);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return users;
    }

     /**
     * This method will insert a user based on the user object to the specified
     * database from the database connection parameter.
     *
     * @since 20180524
     * @author BJM
     *
     * @param DatabaseConnection
     * @param User 
     * @return the associated user type code (0 if not found)
     */
    
    public static synchronized boolean insert(DatabaseConnection databaseConnection, User user) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        boolean results = true;

        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);
            String now = Utility.getNow("yyyy-MM-dd HH:mm:ss");

            sql = "INSERT INTO `User`(`userId`, `username`, `password`, lastName, firstName, additional1, additional2, `userTypeCode`, `createdDateTime`)"
                    + "VALUES (?,?,?,?,?,?,?,?,'"+now+"')";

            if(user.getUserId() == null){
                user.setUserId(0);
            }
            
            ps = conn.prepareStatement(sql);
            ps.setInt(1, user.getUserId());
            ps.setString(2, user.getUsername());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getLastName());
            ps.setString(5, user.getFirstName());
            ps.setString(6, "");
            ps.setString(7, "");
            ps.setInt(8, user.getUserTypeCode());
            ps.execute();
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            results = false;
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }
        return results;

    }

     /**
     * This method will update a user based on the user object to the specified
     * database from the database connection parameter.
     *
     * @since 20180524
     * @author BJM
     *
     * @param DatabaseConnection
     * @param User 
     * @return the associated user type code (0 if not found)
     */

    public static synchronized boolean update(DatabaseConnection databaseConnection, User user) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);

            sql = "UPDATE `User` SET `username` = '" + user.getUsername() + "', `password` = '" + user.getPassword() 
                    + "', `lastName` = " + user.getLastName()+ "', `firstName` = " + user.getFirstName()
                    + "', additional1 = " + user.getAdditional1() + "', additional2 = " + user.getAdditional2()
                    + "', `userTypeCode` = " + user.getUserTypeCode() + " WHERE userId = " + user.getUserId();
            System.out.println(sql);
            ps = conn.prepareStatement(sql);
            ps.execute();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        return true;
    }

     /**
     * This method will delete a user based on the userId parameter value from 
     * the specified database from the database connection parameter.
     *
     * @since 20180524
     * @author BJM
     *
     * @param DatabaseConnection
     * @param int userId
     * @return the associated user type code (0 if not found)
     */
    
    public static synchronized boolean delete(DatabaseConnection databaseConnection, int userId) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);

            sql = "DELETE FROM User WHERE userId = " + userId;

            ps = conn.prepareStatement(sql);
            ps.execute();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        return true;
    }

     /**
     * This method will select a user based on the userId parameter value from 
     * the specified database from the database connection parameter.
     *
     * @since 20180524
     * @author BJM
     *
     * @param DatabaseConnection
     * @param int userId
     * @return the user object from the database
     */

    
    public static synchronized User selectUser(DatabaseConnection databaseConnection, int userId) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        ResultSet rs = null;
        User userSelect = new User();
        System.out.println(userId);

        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);

            sql = "SELECT * FROM `UserAccess` WHERE userAccessId = " + userId;
            System.out.println(sql);
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                userSelect.setUserId(rs.getInt("userId"));
                userSelect.setUsername(rs.getString("username"));
                userSelect.setPassword(rs.getString("password"));
                userSelect.setLastName(rs.getString("lastName"));
                userSelect.setFirstName(rs.getString("firstName"));
                userSelect.setAdditional1(rs.getString("additional1"));
                userSelect.setAdditional2(rs.getString("additional2"));
                userSelect.setUserTypeCode(rs.getInt("userTypeCode"));
                userSelect.setCreatedDateTime(rs.getString("createdDateTime"));
            }

            System.out.println(userSelect.getUsername());
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        return userSelect;
    }
}
